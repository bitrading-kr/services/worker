const mongoose = require('mongoose'),
      { UserSchema, OrderSchema, TradingSchema, StateSchema, LogSchema } = require('./schema.js'),
      _ = require('lodash'),
      { decode, sign, verify } = require('jsonwebtoken'),
      { autoIncrement } = require('mongoose-plugin-autoinc');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

class Db {
  constructor (host, port, user, password, db='admin') {
    let self = this;
    let locked = false;
    let timeId = setInterval(function () {
      if (locked == false) {
        locked = true;
        self.connect(host, port, user, password, db)
          .then(res => {
            self.db = res;
            clearInterval(timeId);
          })
          .catch(err => {
            console.log(`DB Connection Error!!`);
            console.log(err);
            locked = false;
          });
      }
    }, 5000);
  }

  async connect (host, port, user, password, db='admin') {
    let uri = `mongodb://${user}:${password}@${host}:${port}/${db}`;
    db = await mongoose.connect(uri, { useNewUrlParser: true });
    return db;
  }

  async auth (token) { // 사용자 확인
    let data = decode(token);
    let id = data.id,
        access_key = data.access_key;


    let user = this.user;
    let result = await user.findOne({
      id: id,
      "auth.access_key": access_key
    }, "auth.secret_key");
    let secret_key = _.get(result, 'auth.secret_key');

    try {
      verify(token, secret_key);
      return id;
    } catch (e) {
      return false;
    }
  }

  /** users */
  async getUserIds () {
    let user = this.user;
    let ids = await user.find({}, "id");
    return ids.map(elem => elem.id);
  }

  /** apis */
  async updateApi (user_id, exchange, apiObj) {
    let user = this.user;
    let apiPath = `api.${exchange.toLowerCase()}`;
    let setData = _.set({}, apiPath, apiObj);
    let result = await user.updateOne({
      id: user_id
    }, {
      $set: setData
    });
    return result;
  }

  async getApis (user_id) {
    let user = this.user;
    let result = await user.find({id: user_id});
    return _.get(result, '0.api');
  }

  async getApi (user_id, exchange='') {
    let user = this.user;
    let apiPath = `api.${exchange.toLowerCase()}`;
    let result = await user.findOne({
      id: user_id
    }, apiPath);

    return _.get(result, apiPath);
  }

  simplifyOrder (orderData) {
    console.log(`entry start`);
    console.log(JSON.stringify(_.get(orderData, 'entry.strategy'), 2, null));
    if (Array.isArray(_.get(orderData, 'entry.strategy'))) {
      orderData.entry.strategy =
        orderData.entry.strategy.map(elem => {
        elem.tradings = elem.tradings.map(trading => trading.id);
        return elem;
      });
    }
    console.log(`entry stop`);
    console.log(`target start`);
    if (Array.isArray(_.get(orderData, 'target.strategy'))) {
      orderData.target.strategy =
        orderData.target.strategy.map(elem => {
        elem.tradings = elem.tradings.map(trading => trading.id);
        return elem;
      });
    }
    console.log(`target stop`);
    console.log(`stoploss start`);
    if (Array.isArray(_.get(orderData, 'stoploss.strategy.tradings'))) {
      orderData.stoploss.strategy.tradings =
        orderData.stoploss.strategy.tradings.map(trading => trading.id);
    }
    console.log(`stoploss stop`);
    return orderData;
  }

  /** orders */
  async insertOrder (user_id, orderData) {
    if (orderData.id == -1) orderData.id = undefined;
    orderData.updated_at = null;
    orderData.finished_at = null;

    let order = this.order;
    let newOrder = new order(orderData);
    return await newOrder.save();
  }

  async updateOrder (user_id, order_id, orderData) {
    let order = this.order;
    let exists = await order.findOne({
      user_id: user_id,
      id: order_id
    });

    let result;
    if (exists == null || order_id == -1) {
      result = await this.insertOrder(user_id, orderData);
    } else {
      result = await order.updateOne({
        user_id: user_id,
        id: order_id
      }, {
        $set: orderData
      });
    }

    return result;
  }

  async deleteOrder (user_id, order_id) {
    let order = this.order;
    let result = await order.deleteMany({
      user_id: user_id,
      id: order_id
    });

    return result;
  }

  async getOrders (user_id, order_id = null) {
    let order = this.order;
    let query = {};
    let result;
    if (order_id) {
      query = { user_id: user_id, id: order_id };
      result = await order.findOne(query);
    } else {
      query = { user_id: user_id };
      result = await order.find(query);
    }

    return result;
  }

  /** tradings */
  async insertTrading (user_id, tradingData) {
    if (tradingData.id == -1) tradingData.id = undefined;
    tradingData.checked_at = null;
    tradingData.updated_at = null;

    let trading = this.trading;
    let newTrading = new trading(tradingData);
    return await newTrading.save();
  }

  async updateTrading (user_id, trading_id, tradingData) {
    let trading = this.trading;
    let exists = await trading.findOne({
      user_id: user_id,
      id: trading_id
    });

    let result;
    if (exists == null || trading_id == -1) {
      result = await this.insertTrading(user_id, tradingData);
      return result.id;
    } else {
      result = await trading.updateOne({
        user_id: user_id,
        id: trading_id
      }, {
        $set: tradingData
      });
      return trading_id;
    }
  }

  async getTradings (user_id, trading_id=null, options) {
    let trading = this.trading;
    let query = {};
    let result;
    if (trading_id) {
      query = { user_id: user_id, id: trading_id };
      result = await trading.findOne(query);
    } else {
      query = trading.find();
      query.where('user_id').equals(user_id);

      if (!isNaN(options.limit)) {
        query.limit(parseInt(options.limit));
      }

      if (Array.isArray(options.ids))
        query.where('id').in(options.ids);

      query.sort('-id');

      result = await query.exec();
    }

    return result;
  }


  /** states */
  async insertState (user_id, data) {
    let state = this.state;
    let newState = new state({
      user_id: user_id,
      data: data
    });
    return await newState.save();
  }

  async getStates (user_id) {
    let state = this.state;
    let result = await state.find({user_id: user_id}, 'data');
    return result.map(elem => elem.data);
  }

  async getLatestState (user_id) {
    let state = this.state,
        query = { id: user_id };

    let result = await state.find({}).sort({created_at: -1});

    result = result[0];
    return {
      data: _.get(result, 'data'),
      created_at: _.get(result, 'created_at'),
    };
  }
  /** logs */
  async insertLog (user_id, data) {
    let log = this.log;
    let newLog = new log({
      user_id: user_id,
      data: data
    });
    return await newLog.save();
  }

  get counter () {
    if (this._counter != null) return this._counter;

    return this._counter;
  }

  get user () {
    if (this._user == null) {
      this._user = mongoose.model('user', UserSchema);
    }
    return this._user;
  }

  get order () {
    if (this._order == null) {
      OrderSchema.plugin(autoIncrement, {
        model: 'order',
        field: 'id',
        startAt: 1,
        incrementBy: 1
      });
      this._order = mongoose.model('order', OrderSchema);
    }
    return this._order;
  }

  get trading () {
    if (this._trading == null) {
      TradingSchema.plugin(autoIncrement, {
        model: 'trading',
        field: 'id',
        startAt: 1,
        incrementBy: 1
      });
      this._trading = mongoose.model('trading', TradingSchema);
    }
    return this._trading;
  }

  get state () {
    if (this._state == null) {
      this._state = mongoose.model('state', StateSchema);
    }
    return this._state;
  }

  get log () {
    if (this._log == null) {
      this._log = mongoose.model('log', LogSchema);
    }
    return this._log;
  }

}

module.exports = Db;
