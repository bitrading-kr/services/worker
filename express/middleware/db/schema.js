const mongoose = require('mongoose');

module.exports = {
  UserSchema: mongoose.Schema({
    id: { type: String, required: true, unique: true },
    auth: {
      access_key: { type: String },
      secret_key: { type: String }
    },
    api: {
      upbit: {
        access_key: { type: String },
        secret_key: { type: String },
      }
    }
  }),
  OrderSchema: mongoose.Schema({
    version: { type: String, default: 1 },
    id: { type: Number, required: true, unique: true },
    user_id: { type: String, required: true },
    title: { type: String, default: '' },
    desc: { type: String, default: '' },
    author: { type: String, default: '' },
    sourceId: { type: Number },
    exchange: { type: String, required: true },
    coin: { type: String, required: true },
    market: { type: String, required: true },
    created_at: { type: String, required: true },
    updated_at: { type: String },
    finished_at: { type: String },
    state: { type: String, required: true },
    status: { type: String, required: true },
    running_state: { type: String, required: true },
    entry: {
      quantity: { type: Number, required: true },
      strategy: { type: Object, required: true }
    },
    target: {
      strategy: { type: Object, required: true }
    },
    stoploss: {
      type: { type: Number, required: true },
      strategy: { type: Object }
    }
  }),
  TradingSchema: mongoose.Schema({
    version: { type: String, default: 1 },
    id: { type: Number, required: true, unique: true },
    order_id: { type: Number },
    user_id: { type: String },
    state: { type: String },
    side: { type: String },
    state: { type: String },
    exchange: { type: String, required: true },
    coin: { type: String, required: true },
    market: { type: String, required: true },
    info: { type: Object },
    created_at: { type: String },
    checked_at: { type: String },
    updated_at: { type: String },
    fee: {
      executed: { type: Number },
      total: { type: Number }
    },
    volume: {
      executed: { type: Number },
      total: { type: Number }
    },
    price: {
      asking: { type: Number },
      executed: { type: Number },
      locked: { type: Number }
    },
  }),
  StateSchema: mongoose.Schema({
    user_id: { type: String, required: true },
    created_at: { type: Date, required: true, default: Date.now },
    data: { type: Object }
  }),
  LogSchema: mongoose.Schema({
    user_id: { type: String, required: true },
    created_at: { type: Date, required: true, default: Date.now },
    data: { type: Object }
  })
};
