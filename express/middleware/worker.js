const DB = require('./db/index.js');
const sleep = require('await-sleep');
const _ = require('lodash');
const OrderManager = require('@bitrading-worker/order-manager');
const Executor = require('@bitrading-worker/executor');

class Worker {
  constructor () {
    this.db = new DB(process.env.DB_HOST, process.env.DB_PORT, process.env.DB_USERNAME, process.env.DB_PASSWORD, process.env.DB_DATABASE);
  }

  async getUserId (token) {
    let id = await this.db.auth(token);
    if (id == false) throw new Error(`Failed to authorization`);
    return id;
  }

  async updateApi (token, exchange, apiObj) {
    let id = await this.getUserId(token);
    return await this.db.updateApi(id, exchange, apiObj);
  }

  async deleteApi (token, exchange) {
    let id = await this.getUserId(token);
    switch (_.toLower(exchange)) {
    case 'upbit':
      return await this.db.updateApi(id, exchange, {access_key: '', secret_key: ''});
      break;
    default:
      return await this.db.updateApi(id, exchange, {});
      break;
    }
  }

  async getApi (token, exchange='') {
    // exchange 에 따라서 반환값이 다름
    let id = await this.getUserId(token);
    let api = await this.db.getApi(id, exchange);

    switch (exchange.toLowerCase()) {
    case 'upbit':
      return api.access_key;
      break;
    default:
      return null;
      break;
    }
  }

  /** orders */
  async addOrder (token, orderData) {
    let id = await this.getUserId(token);
    let data = await this.db.insertOrder(id, orderData);
    return data;
  }

  async updateOrder (token, order_id, orderData) {
    let id = await this.getUserId(token);
    let data = await this.db.updateOrder(id, order_id, orderData);
    return data;
  }

  async deleteOrder (token, order_id) {
    let id = await this.getUserId(token);
    let data = await this.db.deleteOrder(id, order_id);
    return data;
  }

  async getOrders (token, order_id) {
    let id = await this.getUserId(token);
    let data = await this.db.getOrders(id, order_id);
    return data;
  }

  async activateOrder (token, order_id) {
  }

  async inactivateOrder (token, order_id) {
  }

  /** tradings */
  async getTradings (token, trading_id, options) {
    let id = await this.getUserId(token);
    let data = await this.db.getTradings(id, trading_id, options);
    return data;
  }

  async cancelTrading (token, trading_id) {
  }

  async logging (message, count) {
    if (count % 15 == 0) {
      console.log(message);
    }
  }

  /** state */
  async getOneState (token) {
    let id = await this.getUserId(token);
    let data = await this.db.getLatestState(id);
    return data;
  }

  async getOrdersWithTradings (userId) {
    /** tradings 에 있는 tradingId를 tradingData로 변환 */
    let orders = await this.db.getOrders(userId);
    for (var orderIndex in orders) {
      var order = orders[orderIndex];
      /** entry */
      for (var entryIndex in order.entry.strategy) {
        var entryStrategy = order.entry.strategy[entryIndex];
        for (var tradingIndex in entryStrategy.tradings) {
          var tradingId = entryStrategy.tradings[tradingIndex];

          /** trading DB에서 데이터 가져오기 */
          let tradingData = await this.db.getTradings(userId, tradingId);
          entryStrategy.tradings[tradingIndex] = tradingData;
        }
        order.entry.strategy[entryIndex] = entryStrategy;
      }

      /** target */
      for (var targetIndex in order.target.strategy) {
        var targetStrategy = order.target.strategy[targetIndex];
        for (var tradingIndex in targetStrategy.tradings) {
          var tradingId = targetStrategy.tradings[tradingIndex];

          /** trading DB에서 데이터 가져오기 */
          let tradingData = await this.db.getTradings(userId, tradingId);
          targetStrategy.tradings[tradingIndex] = tradingData;
        }
        order.target.strategy[targetIndex] = targetStrategy;
      }

      /** stoploss */
      if (Array.isArray(_.get(order.stoploss, 'strategy.tradings'))) {
        for (var slIndex in order.stoploss.strategy.tradings) {
          var slTradings = order.stoploss.strategy.tradings[slIndex];
          for (var slTradingIndex in slTradings) {
            var tradingId = slTradings[slTradingIndex];
            let tradingData = await this.db.getTradings(userId, tradingId);
            slTradings[slTradingIndex] = tradingData;
          }
          order.stoploss.strategy.tradings[slIndex] = slTradings;
        }
      }

      orders[orderIndex] = order;
    }
    return orders;
  }

  /** Running */
  async runMain (userId, count) {
    let self = this;
    self.logging(`Count: ${count}`, count);
    try {
      /**
       * 루프 시작
       */
      /** 사용자 API 정보 가져오기 */
      let api = await self.db.getApis(userId);
      let exchanges = Object.keys(api);
      if (exchanges.length == 0) throw new Error(`Not exists supported exchanges('${userId}')`);

      /** Order 정보 가져오기 - DB 쿼리*/
      let orderList = await self.getOrdersWithTradings(userId);
      let om = new OrderManager(orderList);
      let activeOrders = om.filterBy('state', 'active'); // 현재 active 상태인 오더만 가져옴
      // TODO: orderList 관련 처리

      /** 거래소 별로 API 설정하기 */
      let executors = [];
      for (var index in exchanges) {
        var exchange = exchanges[index];
        var apiObj = api[exchange];
        if (exchange == '$init') continue;
        if (typeof exchange !== 'string') throw new TypeError(`Invalid type of exchange name`);
        exchange = _.toUpper(exchange);
        self.logging(`'${exchange}' Start exchange sequence`, count);

        /** Executor 설정 */
        try {
          var executor = new Executor(exchange, apiObj);
          executors.push(executor);
        } catch (executorBindingError) {
          console.log(executorBindingError);
          self.logging(`Failed to binding: '${exchange}'`, count);
        }

        /** States 입력 */
        let status = await executor.runStatus();
        await self.db.insertState(userId, _.set({}, exchange, status));

        /** Logging 입력 */
        if (count % 3600 == 0) { // 1시간에 1번
          self.logging(`Write logging at: ` + new Date(), count);
          await self.db.insertLog(userId, _.set({}, exchange, status));
        }

        /** Executor 실행 */
        await executor.runOrder(activeOrders, {
          update: async function (order) {
            /** entry tradings */
            await order.eachEntries(async function (data) {
              let tradings = data.tradings;
              let newTradings = [];
              for (var trading of tradings) {
                trading.order_id = order.id;
                /** trading db 저장 */
                let tradingId = await self.db.updateTrading(userId, trading.id, trading.trading);
                newTradings.push(tradingId);
              }
              data.tradings = newTradings;
              return data;
            });

            /** target tradings */
            await order.eachTargets(async function (data) {
              let tradings = data.tradings;
              let newTradings = [];
              for (var trading of tradings) {
                trading.order_id = order.id;
                /** trading db 저장 */
                let tradingId = await self.db.updateTrading(userId, trading.id, trading.trading);
                newTradings.push(tradingId);
              }
              data.tradings = newTradings;
              return data;
            });

            /** stoploss tradings */
            if (Array.isArray(_.get(order.stoploss, 'strategy.tradings'))) {
              let stoplossTradings = [];
              for (var trading of order.stoploss.strategy.tradings) {
                let tradingId = await self.db.updateTrading(userId, trading.id, trading.trading);
                stoplossTradings.push(tradingId);
              }
              order.stoploss.strategy.tradings = stoplossTradings;
            }

            /** order db 저장(사용자 input으로 설정될 수 있는 값은 제거) */
            // state 설정 (done은 executor에서 설정될 수 있음)
            if (order.order.state != 'done') delete order.order.state;
            await self.db.updateOrder(userId, order.id, order.order);
          }
        });

      }
    } catch (err) {
      self.logging(`Error occured ` + new Date(), count);
      console.log(err);
    }

  }

  async run () { // main run
    try {
      let self = this;

      /** 사용자 ID 가져오기 */
      let userIds = await this.db.getUserIds();
      let userId = userIds[0];
      if (userId == null) throw new Error(`User ID does not exists`);

      let runningCount = 0;
      let locked = false;
      // 1초에 1번 실행
      setInterval( async function () {
        if (locked == false) {
          locked = true;
          runningCount ++;
          try {
            self.runMain(userId, runningCount);
          } catch (err) {
            console.log(userId + ' Error occured ');
            console.log(err);
          }

          locked = false;
        }
      }, 3000);
    } catch (err) {
      console.log(err);
    }
  }
}

module.exports = new Worker();
