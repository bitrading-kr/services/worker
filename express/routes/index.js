var express = require('express');
var router = express.Router();
var worker = require('../middleware/worker.js');
const axios = require('axios');
const _ = require('lodash');

/** 기능 작동 오류 나도 지속적으로 돌아감. 모든 값은 DB에 저장, API는 DB에서 값을 요청*/
worker.run();

// worker.addOrder('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InRlc3Rlcl9zYW0iLCJhY2Nlc3Nfa2V5IjoiNTdBZUgzYVVQcG82bUZyVkQxWGNrVjFhNlNMdEZpMnpIYzJLb1c5M2ZzeVFUQ2VFOCIsImlhdCI6MTU0MTQ4ODMxNX0.VJsyaIdt1DxZsqoQ4DaeR-eQ38uanRqXZ8gmBMYtOsM', {
//   version: '1',
//   user_id: 'tester_sam',
//   title: '',
//   desc: '',
//   source_id: -1,
//   exchange: 'UPBIT',
//   market: 'BTC',
//   coin: 'PART',
//   created_at: '2018-11-11 17:45:14.374 +09:00',
//   updated_at: '2018-11-11 08:45:14.383 +00:00',
//   finished_at: '2018-11-11 08:45:14.384 +00:00',
//   state: 'inactive',
//   status: 'stuck',
//   running_state: 'profit',
//   entry: { quantity: 34, strategy: [] },
//   target: { strategy: [] },
//   stoploss: { type: 1, strategy: [] }
// } );

function getToken (headers) {
  let token = _.get(headers, 'worker-token');
  if (token == null) throw new Error("Token ins not exists");
  return token;
}

async function handler (req, res, func) {
  try {
    console.log(req.headers);
    let token = getToken(req.headers);
    let result = await func(token, _.get(req, 'params'), _.get(req, 'body'));
    res.send(result).end();
  } catch (err) {
    console.log(err);
    res.status(500).send(err.message).end();
  }
}

router.route('/orders')
  .get(function (req, res, next) { // DONE
    handler(req, res, async function (token, params, data) {
      let result = await worker.getOrders(token);
      return result;
    });
  })
  .post(function (req, res, next) { // DONE
    handler(req, res, async function (token, params, data) {
      let result = await worker.addOrder(token, data);
      return result;
    });
  });

router.route('/orders/:id')
  .get(function(req, res, next) { // DONE
    handler(req, res, async function (token, { id }, data) {
      let result = await worker.getOrders(token, id);
      return result;
    });
  })
  .put(function(req, res, next) { // DONE
    handler(req, res, async function (token, { id }, data) {
      let result = await worker.updateOrder(token, id, data);
      return result;
    });
  })
  .delete(function(req, res, next) { // DONE
    handler(req, res, async function (token, { id }, data) {
      let result = await worker.deleteOrder(token, id);
      return result;
    });
  });

/**
 * tradings
 */
router.route('/tradings')
  .get(function (req, res, next) {
    /**
     * query
     * ids : Array
     * limit : Number
     */
    let query = req.query;
    handler(req, res, async function (token, params, data) {
      let ids = query.ids,
          limit = query.limit; // 배열

      let result = [];
      result = await worker.getTradings(token, null, query);
      return result;
    });
  });

router.get('/tradings/:id', function (req, res, next) {
});

/**
 * commands
 */
router.get('/commands/orders', function (req, res, next) {
});

router.get('/commands/tradings', function (req, res, next) {
});

/**
 * api
 */
router.route('/apis/:exchange')
  .get(function (req, res, next) {
    handler(req, res, async function (token, {exchange}, data) {
      let api = await worker.getApi(token, exchange);
      return api;
    });
  })
  .put(async function (req, res, next) {
    handler(req, res, async function (token, {exchange}, data) {
      let api = await worker.updateApi(token, exchange, {
        access_key: data.access_key,
        secret_key: data.secret_key
      });
      return api.ok == 1;
    });
  })
  .delete(async function (req, res, next) {
    handler(req, res, async function (token, {exchange}, data) {
      await worker.deleteApi(token, exchange);
      return true;
    });
  });

router.get('/states', function (req, res, next) {
  handler(req, res, async function (token, params, data) {
    return await worker.getOneState(token);
  });
});

module.exports = router;
